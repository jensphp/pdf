<?php

require_once \dirname(__DIR__) . \DIRECTORY_SEPARATOR . 'vendor' . \DIRECTORY_SEPARATOR . 'autoload.php';

(new \jj\Pdf\Pdf(\Symfony\Component\HttpFoundation\Request::createFromGlobals()))->output();
