<?php

namespace jj\Pdf;

use GuzzleHttp\RequestOptions;
use jj\Pdf\Exception\Param\Required;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Pdf
 *
 * @package jj\Pdf
 */
class Pdf
{
    /**
     * @var bool
     */
    private $isGenerated = false;
    /**
     * @var string
     */
    private $pdf = '';
    /**
     * @var array
     */
    private $ads = [];
    /**
     * @var int
     */
    private $adPage = 20;
    /**
     * @var bool
     */
    private $debug = false;
    /**
     * @var string|null
     */
    private $content = null;
    /**
     * @const string
     */
    private const PARAM_PDF = 'pdf';
    /**
     * @const string
     */
    private const PARAM_ADS = 'ads';
    /**
     * @const string
     */
    private const PARAM_AD_PAGE = 'adPage';

    /**
     * Pdf constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->setPdf($request->get(self::PARAM_PDF, ''));
        $this->setAds($request->get(self::PARAM_ADS, []));
        $this->setAdPage($request->get(self::PARAM_AD_PAGE, 20));
        $this->setDebug($request->query->has('debug'));

        $this->log($request->getRequestUri());
    }

    /**
     * @return bool
     */
    protected function isGenerated(): bool
    {
        return $this->isGenerated;
    }

    /**
     * @param bool $isGenerated
     *
     * @return Pdf
     */
    protected function setIsGenerated(bool $isGenerated): Pdf
    {
        $this->isGenerated = $isGenerated;

        return $this;
    }

    /**
     * @return string
     */
    protected function getPdf(): string
    {
        return $this->pdf;
    }

    /**
     * @param mixed $pdf
     *
     * @return Pdf
     */
    protected function setPdf(string $pdf): Pdf
    {
        if (!$pdf) {
            throw new Required('pdf');
        }
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * @return array
     */
    protected function getAds(): array
    {
        return $this->ads;
    }

    /**
     * @param array $ads
     *
     * @return Pdf
     */
    protected function setAds(array $ads): Pdf
    {
        $this->ads = $ads;

        return $this;
    }

    /**
     * @return int
     */
    protected function getAdPage(): int
    {
        return $this->adPage;
    }

    /**
     * @param int $adPage
     *
     * @return Pdf
     */
    protected function setAdPage(int $adPage): Pdf
    {
        if ($adPage < 3) {
            throw new Exception('min value for adPage is 3');
        }
        $this->adPage = $adPage;

        return $this;
    }

    /**
     * @return bool
     */
    protected function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     *
     * @return Pdf
     */
    protected function setDebug(bool $debug): Pdf
    {
        $this->debug = $debug;

        return $this;
    }

    /**
     * @return string|null
     */
    protected function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return Pdf
     */
    protected function setContent(?string $content): Pdf
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    protected function getTmpFolder(): string
    {
        return \dirname(__DIR__, 3) . \DIRECTORY_SEPARATOR . 'tmp';
    }

    /**
     * @return string
     */
    protected function getLogFolder(): string
    {
        return \dirname(__DIR__, 3) . \DIRECTORY_SEPARATOR . 'log';
    }

    /**
     * @return string
     */
    protected function getDataFolder(): string
    {
        return \dirname(__DIR__, 3) . \DIRECTORY_SEPARATOR . 'data';
    }

    /**
     * @return array
     */
    protected function getWatermarks(): array
    {
        return \array_reverse(\glob($this->getDataFolder() . \DIRECTORY_SEPARATOR . 'watermark' . \DIRECTORY_SEPARATOR . '*.pdf'));
    }

    /**
     * @return array
     */
    protected function getCovers(): array
    {
        return \array_reverse(\glob($this->getDataFolder() . \DIRECTORY_SEPARATOR . 'cover' . \DIRECTORY_SEPARATOR . '*.pdf'));
    }

    /**
     * @param string $type
     *
     * @return array
     * @throws \Exception
     */
    protected function downloadFiles(string $type): array
    {
        $localFileTemplate = $this->getDataFolder() . \DIRECTORY_SEPARATOR . $type . \DIRECTORY_SEPARATOR . '%s.pdf';

        $ads = [];

        $method = 'get' . \ucfirst($type);
        foreach ($this->$method() as $fileUri => $fileHash) {
            $localFile = null;
            if ($fileHash) {
                $localFile = \sprintf($localFileTemplate, preg_replace("/[^a-zA-Z0-9\s]/", "", $fileHash));
            }

            if (!$localFile || !\is_file($localFile)) {
                $r = (new \GuzzleHttp\Client)->get($fileUri, [
                    RequestOptions::VERIFY => false,
                ]);
                if ($r->getStatusCode() !== 200) {
                    throw new \Exception('Status code for request to uri "' . $this->getPdf() . '" is "' . $r->getStatusCode() . '" and not 200 ');
                }

                if (!$localFile) {
                    $localFile = \sprintf($localFileTemplate, \md5((string)$r->getBody()));
                    if (\is_file($localFile)) {
                        $ads[] = $localFile;
                        continue;
                    }
                }

                if (\file_put_contents($localFile, $r->getBody()) === false) {
                    throw new \Exception('Could not create file "' . $localFile . '".');
                }
            }
            $ads[] = $localFile;
        }

        return $ads;
    }

    /**
     * @return Pdf
     * @throws \Exception
     */
    protected function generate(): Pdf
    {

        if (!$this->isGenerated()) {
            $r = (new \GuzzleHttp\Client)->get($this->getPdf(), [
                RequestOptions::VERIFY => false,
            ]);

            if ($r->getStatusCode() !== 200) {
                throw new \Exception('Status code for request to uri "' . $this->getPdf() . '" is "' . $r->getStatusCode() . '" and not 200 ');
            }

            $pdfFile = $this->getTmpFolder() . \DIRECTORY_SEPARATOR . \date('Y-m-d-H-i-s') . \uniqid('-' . \md5($this->getPdf()) . '-') . '-%d.pdf';
            $counter = 0;

            if (\file_put_contents(\sprintf($pdfFile, $counter), $r->getBody()) === false) {
                throw new \Exception('Could not create file "' . \sprintf($pdfFile, $counter) . '".');
            }

            # Watermarken
            foreach ($this->getWatermarks() as $watermark) {
                $this->exec('pdftk ' . \escapeshellarg(\sprintf($pdfFile, $counter)) . '  background ' . \escapeshellarg($watermark) . '  output ' . \escapeshellarg(\sprintf($pdfFile, ++$counter)));
            }

            # Ads
            $ads = $this->downloadFiles(self::PARAM_ADS);
            if (\count($ads)) {
                $pages = $this->exec('pdftk ' . \escapeshellarg(\sprintf($pdfFile, $counter)) . ' dump_data | grep NumberOfPages');

                $pages = (int)\str_replace('NumberOfPages: ', '', $pages[0]);
                $insertAfterXPages = $this->getAdPage();

                if ($insertAfterXPages < $pages) {
                    // Nur machen, wenn das PDF mehr Seite hat als $insertAfterXPages
                    $adPdfs = [];
                    $adKeys = [];
                    foreach ($ads as $index => $ad) {
                        $adKey = \str_repeat('B', $index + 1);
                        $adPdfs[] = $adKey . '=' . \escapeshellarg($ad);
                        $adKeys[] = $adKey;
                    }

                    $count = 0;
                    while (true) {
                        $ad = current($adKeys);
                        if (!next($adKeys)) {
                            reset($adKeys);
                        }

                        $start = (++$count - 1) * $insertAfterXPages + 1;
                        $end = $count * $insertAfterXPages;

                        if ($end > $pages) {
                            $cuts[] = 'A' . $start . '-end';
                            break;
                        } else {
                            $cuts[] = 'A' . $start . '-' . $end;
                            $cuts[] = $ad . '1-end';

                            if ($end === $pages) {
                                break;
                            }
                        }
                    }

                    $this->exec('pdftk A=' . \escapeshellarg(\sprintf($pdfFile, $counter)) . ' ' . \implode(' ', $adPdfs) . ' cat ' . \implode(' ', $cuts) . ' output ' . \escapeshellarg(\sprintf($pdfFile, ++$counter)));
                } else {
                    // Ansonsten das erste Ad hinten dran hängen
                    $this->exec('pdftk A=' . \escapeshellarg(\sprintf($pdfFile, $counter)) . ' B=' . \escapeshellarg(\current($ads)) . ' cat A1-end B1-end output ' . \escapeshellarg(\sprintf($pdfFile, ++$counter)));
                }
            }

            # Seiten am Anfang einfügen
            foreach ($this->getCovers() as $cover) {
                $this->exec('pdftk ' . \escapeshellarg($cover) . ' ' . \escapeshellarg(\sprintf($pdfFile, $counter)) . ' cat output ' . \escapeshellarg(\sprintf($pdfFile, ++$counter)));
            }

            $this->setContent(\file_get_contents(\sprintf($pdfFile, $counter)));

            if (!$this->isDebug()) {
                while ($counter >= 0) {
                    if (\is_writable(\sprintf($pdfFile, $counter))) {
                        \unlink(\sprintf($pdfFile, $counter));
                    }
                    $counter--;
                }
            }

            $this->setIsGenerated(true);
        }

        return $this;
    }

    /**
     * @param string $exec
     *
     * @return array
     */
    protected function exec(string $exec): array
    {
        \exec($exec, $output);

        $this->log('exec: ' . $exec);
        $this->log('output: ' . \var_export($output, true));

        return $output;
    }

    /**
     * @param string $log
     *
     * @return Pdf
     */
    protected function log(string $log): Pdf
    {
        if ($this->isDebug()) {
            $h = \fopen($this->getLogFolder() . \DIRECTORY_SEPARATOR . \date('Y-m-d') . '.log', 'a');
            \fwrite($h, \date('H:i:s') . ' - ' . $log . \PHP_EOL);
            \fclose($h);
        }

        return $this;
    }

    /**
     * @return Pdf
     * @throws \Exception
     */
    public function output(): Pdf
    {
        try {
            $content = $this->generate()->getContent();
            \header("Content-type:application/pdf");
            echo $content;
        } catch (\Exception $e) {
            $this->log($e->getMessage());

            \http_response_code(500);
            echo $e->getMessage();
        }

        return $this;
    }
}
