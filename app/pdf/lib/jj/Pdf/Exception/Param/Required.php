<?php

namespace jj\Pdf\Exception\Param;

use jj\Pdf\Exception\Param;

/**
 * Class Required
 *
 * @package jj\App\Exception\Param
 */
class Required extends Param
{
}