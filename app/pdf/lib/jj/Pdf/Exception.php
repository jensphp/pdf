<?php

namespace jj\Pdf;

/**
 * Class Exception
 *
 * @package jj\Pdf
 */
class Exception extends \Exception
{
}